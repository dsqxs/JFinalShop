package shop;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.EnumMap;

import org.apache.commons.collections.EnumerationUtils;
import org.apache.commons.lang.enums.EnumUtils;

import com.cms.entity.MessageConfig;
import com.cms.entity.Payment;

public class Test {

    public static void main(String[] args) throws ParseException {
        // TODO Auto-generated method stub
//        BigDecimal a = new BigDecimal("10.00");
//        System.out.println(a.toString());
    	
//    	BigDecimal a = new BigDecimal("1.00");
//    	System.out.println(a.intValue());
//    	Integer a = 1;
//    	Integer b = 1;
//    	System.out.println(a==b);
    	//System.out.println(new String(Base64.getDecoder().decode("RXJyb3IgMTogRmFpbCB0byBvcGVuIHRoZSBzb3VyY2UgZmlsZS4=")));
//    	String a = ProductUtils.qrImage("http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epOniaK1eJy0GSoFhc3jY61hjTUBZQtBoc8KRqwiaenMicXqndG0zNpgCialZa2nbkhFhMG9z7yKNVe8w/132", 
//    			"樱木", 
//    			"http://auqiniu.ozstrong.com/96b5c36c-3ab1-4c5c-8653-b31189dabf4d.png", 
//    			"A2婴儿奶粉2段新版A2婴儿奶粉2段新版", 
//    			"6.50", 
//    			"1");
//    	System.out.println(a);
//    	BigDecimal rate = new BigDecimal("4.938082");
//    	BigDecimal newmoney = new BigDecimal("256").divide(rate,2,RoundingMode.HALF_UP);
//    	DecimalFormat df = new DecimalFormat("#.00");
//    	
//    	BigDecimal newmoney1 = new BigDecimal("43").multiply(rate);
//    	DecimalFormat df1 = new DecimalFormat("#.00");
//    	System.out.println(df1.format(newmoney));
    	
//    	 SmsUtils.sendRegisterMemberSms("1234","19908488901");
    	
//    	System.out.println(EmojiStringUtils.replaceEmoji("\\xF0\\x9F\\xA7\\xB8\\xE0\\xBE"));
    	
//    	BigDecimal rate = new BigDecimal("4.938082");
//    	BigDecimal newmoney = new BigDecimal("0.01").multiply(rate);
//    	DecimalFormat df = new DecimalFormat("0.00");
//    	System.out.println(df.format(newmoney));
//    	System.out.println(new BigDecimal("0").compareTo(BigDecimal.ZERO));
//    	Long a = 0l;
//    	Integer b = 1;
//    	
//    	System.out.println(a+b);
    	
//    	System.out.println(ShopUtils.getAuPrice(new BigDecimal(df.format(newmoney)))+"");
    	
//    	FileLib
    	
//    	String endpoint = "oss-cn-hangzhou.aliyuncs.com";    // Endpoint以杭州为例，其它Region请按实际情况填写。
//        String accessId = "LTAI4G53WtS64QK3RP29Jdui";    //AccessKey ID 
//        String accessKey = "Hgj6F0bEvN82j33D2d520jM4wwcMTQ";   //Access Key Secret    
//        String bucketName = "jreshop";  //桶名称
//        OSSClient ossClient = new OSSClient(endpoint, accessId, accessKey);
//        ObjectListing list = ossClient.listObjects(new ListObjectsRequest(bucketName, null, null, null, 1000));
//        List<OSSObjectSummary> objs = list.getObjectSummaries();
//        for(OSSObjectSummary s : objs){
//        	ossClient.deleteObject(bucketName, s.getKey());
//        }
//    	Map<String,String> map = new HashMap<>();
//    	String aaa = map.get("aaa");
//    	System.out.println(aaa);
//    	EnumMap<Payment.Method, String> map = new EnumMap<>(Payment.Method.class);

        System.out.println(daysBetween("2022-10-25","2023-01-21"));
    }

    public static int daysBetween(String smdate,String bdate) throws ParseException {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(smdate));
        long time1 = cal.getTimeInMillis();
        cal.setTime(sdf.parse(bdate));
        long time2 = cal.getTimeInMillis();
        long between_days=(time2-time1)/(1000*3600*24);

        return Integer.parseInt(String.valueOf(between_days));
    }

}
