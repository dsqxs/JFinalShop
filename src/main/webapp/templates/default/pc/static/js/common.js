//变量是否为空
function isEmpty(v) {
    switch (typeof v) {
    case 'undefined':
        return true;
    case 'string':
        if (v.replace(/(^[ \t\n\r]*)|([ \t\n\r]*$)/g, '').length == 0) return true;
        break;
    case 'boolean':
        if (!v) return true;
        break;
    case 'number':
        if (0 === v || isNaN(v)) return true;
        break;
    case 'object':
        if (null === v || v.length === 0) return true;
        for (var i in v) {
            return false;
        }
        return true;
    }
    return false;
}
//
function seckill(endDate){
	// 当前时间
	var nowTime = new Date();
	// 结束时间  yyyy/MM/dd HH:mm:ss
	var endTime = new Date(endDate);
	// 相差的时间	
	var t = endTime.getTime() - nowTime.getTime();
	if (t <= 0) {
	    return false;
	}
	var d = Math.floor(t / 1000 / 60 / 60 / 24);
	var h = Math.floor(t / 1000 / 60 / 60 % 24);
	var i = Math.floor(t / 1000 / 60 % 60);
	var s = Math.floor(t / 1000 % 60);
	document.getElementById('countdownDay').innerHTML = d;
	document.getElementById('countdownHour').innerHTML = h;
	document.getElementById('countdownMinute').innerHTML = i;
	document.getElementById('countdownSecond').innerHTML = s;
	setInterval(seckill, 1000);
}

(function($) {

    $.fn.extend({
        // 文件上传
        uploader: function(options) {
            var settings = {
                url: '/member/file/upload',
                fileName: "file",
                data: {},
                multiple:false,
                maxSize: 10,
                extensions: "jpg,jpeg,bmp,gif,png,swf,flv,mp3,wav,avi,rm,rmvb,zip,rar,7z,doc,docx,xls,xlsx,ppt,pptx",
                before: null,
                complete: null
            };
            $.extend(settings, options);
            var $progressBar = $('<div class="progressBar"><\/div>').appendTo("body");
            return this.each(function() {
                var element = this;
                var $element = $(element);

                var webUploader = WebUploader.create({
                    swf: '/admin/static/plugins/webuploader/webuploader.swf',
                    server: settings.url,
                    pick: {
                        id: element,
                        multiple: false
                    },
                    fileVal: settings.fileName,
                    formData: settings.data,
                    fileSingleSizeLimit: settings.maxSize * 1024 * 1024,
                    accept: {
                        extensions: settings.extensions
                    },
                    fileNumLimit: 1,
                    auto: true
                }).on('beforeFileQueued', function(file) {
                    if ($.isFunction(settings.before) && settings.before.call(element, file) === false) {
                        return false;
                    }
                    if ($.trim(settings.extensions) == '') {
                        this.trigger('error', 'Q_TYPE_DENIED');
                        return false;
                    }
                    this.reset();
                    $progressBar.show();
                }).on('uploadProgress', function(file, percentage) {
                    $progressBar.width(percentage * 100 + '%');
                }).on('uploadAccept', function(file, data) {
                    $progressBar.fadeOut("slow", function() {
                        $progressBar.width(0);
                    });
                    if (data.state != 'SUCCESS') {
                        alert(data.message);
                        return false;
                    }
                    fileUploadCall(settings.fromType,data.url);
                    //if(fromType=="common" || fromType == null){
                    //$("#fileshow").html('<img class="img" src="'+data.url+'" alt="pic" height="110">')
                    /* if(settings.multiple){
                        var multipleFileParent = $element.parent().parent();
                        var name = multipleFileParent.attr("data-name");
                        multipleFileParent.find(".multipleFile").append('<div><a href="javascript:;">'+data.url+'&nbsp;x</a><input type="hidden" name="'+name+'" value="'+data.url+'"/></div>');
                    }else{
                        $element.parent().parent().find("input:text").val(data.url);
                    } */
                    //}else if(fromType == "product"){
                    //addFile(data.url);
                    /* var html = '<div class="upload-Picitem"><input type="hidden" name="productImages" value="'+data.name+'"><img class="img" src="'+data.url+'" alt="pic"><img onclick="removeImg(this)" class="imgDelete" src="#(base)/admin/static/img/del_img.png"></div>';
                     $("#imgBox").append(html); */
                    /*$element.parent().parent().find(".source").val(data.source);
                   $element.parent().parent().find(".large").val(data.large);
                   $element.parent().parent().find(".medium").val(data.medium);
                   $element.parent().parent().find(".thumbnail").val(data.thumbnail); */
                    //}
                    if ($.isFunction(settings.complete)) {
                        settings.complete.call(element, file, data);
                    }
                }).on('error', function(type) {
                    switch(type) {
                        case "F_EXCEED_SIZE":
                            alert("上传文件大小超出限制");
                            break;
                        case "Q_TYPE_DENIED":
                            alert("上传文件格式不正确");
                            break;
                        default:
                            alert("上传文件出现错误");
                    }
                });

                $element.mouseover(function() {
                    webUploader.refresh();
                });
            });
        }
    });
})(jQuery);