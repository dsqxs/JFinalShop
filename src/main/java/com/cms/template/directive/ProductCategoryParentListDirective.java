package com.cms.template.directive;

import java.util.List;

import com.cms.TemplateVariable;
import com.cms.entity.ProductCategory;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;

/**
 * 模板指令 - 上级商品分类列表
 * 
 * 
 * 
 */
@TemplateVariable(name="product_category_parent_list")
public class ProductCategoryParentListDirective extends BaseDirective {

	/** "商品分类ID"参数名称 */
	private static final String PRODUCT_CATEGORY_ID_PARAMETER_NAME = "productCategoryId";
	
	/** "是否递归"参数名称 */
	private static final String RECURSIVE_PARAMETER_NAME = "recursive";

	/** 变量名称 */
	private static final String VARIABLE_NAME = "productCategorys";

    @Override
    public void exec(Env env, Scope scope, Writer writer) {
        // TODO Auto-generated method stub
        scope = new Scope(scope);
        Long productCategoryId = getParameter(PRODUCT_CATEGORY_ID_PARAMETER_NAME, Long.class, scope);
        Boolean recursive = getParameter(RECURSIVE_PARAMETER_NAME, Boolean.class, scope);
        Integer count = getCount(scope);
        List<ProductCategory> productCategorys = new ProductCategory().dao().findParents(productCategoryId, recursive != null ? recursive : false, count);
        scope.setLocal(VARIABLE_NAME,productCategorys);
        stat.exec(env, scope, writer);
    }
    
    public boolean hasEnd() {
        return true;
    }

}