package com.cms.template.directive;

import java.util.List;

import com.cms.TemplateVariable;
import com.cms.entity.ArticleCategory;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;

/**
 * 模板指令 - 上级文章分类列表
 * 
 * 
 * 
 */
@TemplateVariable(name="article_category_parent_list")
public class ArticleCategoryParentListDirective extends BaseDirective {
	/** "文章分类ID"参数名称 */
	private static final String ARTICLECATEGORYID_PARAMETER_NAME = "articleCategoryId";
	
	/** "是否递归"参数名称 */
	private static final String RECURSIVE_PARAMETER_NAME = "recursive";

	/** 变量名称 */
	private static final String VARIABLE_NAME = "articleCategorys";

    @Override
    public void exec(Env env, Scope scope, Writer writer) {
        // TODO Auto-generated method stub
        scope = new Scope(scope);
        Long articleCategoryId = getParameter(ARTICLECATEGORYID_PARAMETER_NAME, Long.class, scope);
        Boolean recursive = getParameter(RECURSIVE_PARAMETER_NAME, Boolean.class, scope);
        Integer count = getCount(scope);
        List<ArticleCategory> articleCategorys = new ArticleCategory().dao().findParents(articleCategoryId,recursive != null ? recursive : false, count);
        scope.setLocal(VARIABLE_NAME,articleCategorys);
        stat.exec(env, scope, writer);
    }

    public boolean hasEnd() {
        return true;
    }
}