package com.cms.util;

import java.util.HashMap;
import java.util.Map;

import com.cms.Config;
import com.cms.entity.MessageConfig;
import com.cms.util.sms.AliSmsUtils;
/**
 * Utils - 短信
 * 
 * 
 * 
 */
public class SmsUtils {
    

	/**
	 * 添加短信发送任务
	 * 
	 * @param templateCode
	 *            模板编码
	 * @param params
	 *            参数
	 * @param toMobiles
	 *            接收人手机号码
	 */
	private static void addSendTask(final String templateCode,final Map<String,Object> params,final String[] toMobiles) {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				send(templateCode,params,toMobiles);
			}
		});
		thread.start();
	}

	/**
	 * 发送短信
	 * 
	 * @param templateCode
	 *            模板编码
	 * @param params
	 *            参数
	 * @param toMobiles
	 *            接收人手机号码
	 */
	private static void send(String templateCode,Map<String,Object> params,String[] toMobiles) {
		AliSmsUtils.send(templateCode, params, toMobiles);
	}

	/**
	 * 发送短信
	 * 
	 * @param templateCode
	 *            模板编码
	 * @param params
	 *            参数
	 * @param toMobiles
	 *            接收人手机号码
	 * @param async
	 *            是否异步
	 */
	public static void send(String templateCode,Map<String,Object> params,String[] toMobiles, boolean async) {
		if (async) {
			addSendTask(templateCode,params,toMobiles);
		} else {
			send(templateCode,params,toMobiles);
		}
	}


	/**
	 * 发送短信(异步)
	 * 
	 * @param templateCode
	 *            模板编码
	 * @param params
	 *            参数
	 * @param toMobile
	 *            接收人手机号码
	 */
	public static void send(String templateCode,Map<String,Object> params,String toMobile) {
		send(templateCode,params,new String[] { toMobile }, true);
	}

	/**
	 * 发送会员注册短信
	 * 
	 * @param member
	 *            会员
	 */
	public static void sendRegisterMemberSms(String code,String toMobile) {
		MessageConfig messageConfig = new MessageConfig().dao().findByType(MessageConfig.Type.REGISTER.ordinal());
		if (messageConfig == null) {
			return;
		}
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("code", code);
//		params.put("product", config.getSiteName());
		
		send(messageConfig.getSmsCode(),params,toMobile);
	}

    /**
     * 发送忘记密码短信
     * 
     * @param member
     *            会员
     */
    public static void sendForgetPasswordSms(String code,String toMobile) {
        MessageConfig messageConfig = new MessageConfig().dao().findByType(MessageConfig.Type.FORGET_PASSWORD.ordinal());
        if (messageConfig == null) {
            return;
        }
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("code", code);
//        params.put("product", config.getSiteName());
        
        send(messageConfig.getSmsCode(),params,toMobile);
    }
    
    /**
     * 发送修改密码短信
     * 
     * @param member
     *            会员
     */
    public static void sendUpdatePasswordSms(String code,String toMobile) {
        MessageConfig messageConfig = new MessageConfig().dao().findByType(MessageConfig.Type.FORGET_PASSWORD.ordinal());
        if (messageConfig == null) {
            return;
        }
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("code", code);
//        params.put("product", config.getSiteName());
        
        send(messageConfig.getSmsCode(),params,toMobile);
    }
    
    /**
     * 设置账户短信
     * 
     * @param member
     *            会员
     */
    public static void sendSetAccountSms(String code,String toMobile) {
        MessageConfig messageConfig = new MessageConfig().dao().findByType(MessageConfig.Type.SET_ACCOUNT.ordinal());
        if (messageConfig == null) {
            return;
        }
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("code", code);
//        params.put("product", config.getSiteName());
        
        send(messageConfig.getSmsCode(),params,toMobile);
    }
    
    
    /**
     * 登录短信
     * 
     * @param member
     *            会员
     */
    public static void sendLoginSms(String code,String toMobile) {
        MessageConfig messageConfig = new MessageConfig().dao().findByType(MessageConfig.Type.LOGIN.ordinal());
        if (messageConfig == null) {
            return;
        }
        Config config = SystemUtils.getConfig();
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("code", code);
//        params.put("product", config.getSiteName());
        
        send(messageConfig.getSmsCode(),params,toMobile);
    }


}
