package com.cms.controller.front;

import java.util.HashMap;

import org.apache.commons.codec.digest.DigestUtils;

import com.cms.Feedback;
import com.cms.entity.Member;
import com.cms.routes.RouteMapping;

/**
 * Controller - 密码
 * 
 * 
 * 
 */
@RouteMapping(url = "/password")
public class PasswordController extends BaseController{
	
	public void index(){
		render("/templates/"+getTheme()+"/"+getDevice()+"/password_one.html");
	}
	
	public void next(){
		String username = getPara("username");
		setAttr("username", username);
		render("/templates/"+getTheme()+"/"+getDevice()+"/password_two.html");
	}
	
	public void forgot(){
		String username = getPara("username");
        String password = getPara("password");
        Member member = new Member().dao().findByMobile(username);
        if(member==null){
            renderJson(Feedback.error("用户不存在"));
            return;
        }else{
            member.setPassword(DigestUtils.md5Hex(password));
            member.update();
            renderJson(Feedback.success(new HashMap<>()));
            return;
        }
	}
}
