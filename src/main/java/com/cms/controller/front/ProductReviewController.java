package com.cms.controller.front;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.cms.Feedback;
import com.cms.entity.ProductReview;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/product_review")
public class ProductReviewController extends BaseController{
	
	public void index(){
		Long productId=getParaToLong(0);
		List<ProductReview> productReviews = new ProductReview().dao().findByProductId(productId);
		setAttr("productReviews", productReviews);
		render("/templates/"+getTheme()+"/"+getDevice()+"/product_review.html");
	}
	
	public void save(){
		ProductReview productReview = getModel(ProductReview.class,"",true); 
		productReview.setCreateDate(new Date());
		productReview.setModifyDate(new Date());
		productReview.save();
		renderJson(Feedback.success(new HashMap<>()));
	}

}
