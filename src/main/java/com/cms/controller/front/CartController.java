package com.cms.controller.front;

import java.util.HashMap;
import java.util.Map;

import com.cms.Feedback;
import com.cms.entity.Cart;
import com.cms.entity.Member;
import com.cms.routes.RouteMapping;

/**
 * Controller - 购物车
 * 
 * 
 * 
 */
@RouteMapping(url = "/cart")
public class CartController extends BaseController{

	
	/**
	 * 列表
	 */
	public void list(){
		Cart currentCart = getCurrentCart();
		setAttr("currentCart",currentCart);
		render("/templates/"+getTheme()+"/"+getDevice()+"/cart_list.html");
	}
	
	
	/**
	 * 查看
	 */
	public void view(){
		Member currentMember = getCurrentMember();
		if(currentMember==null){
			Map<String,Object> data = new HashMap<>();
	        data.put("quantity", 0);
	        data.put("totalPrice", 0);
	        renderJson(Feedback.success(data));
	        return;
		}
	    Cart currentCart = getCurrentCart();
	    Map<String,Object> data = new HashMap<>();
        data.put("quantity", currentCart.getQuantity());
        data.put("totalPrice", currentCart.getTotalPrice());
        renderJson(Feedback.success(data));
	}
	
}
