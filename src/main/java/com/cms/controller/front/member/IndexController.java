package com.cms.controller.front.member;

import java.util.Date;
import java.util.HashMap;

import com.cms.Feedback;
import com.cms.controller.front.BaseController;
import com.cms.entity.Commission;
import com.cms.entity.Member;
import com.cms.entity.Order;
import com.cms.routes.RouteMapping;

/**
 * Controller - 首页
 * 
 * 
 * 
 */
@RouteMapping(url = "/member")
public class IndexController extends BaseController{
	
	
	
	public void index(){
		setAttr("todayDistributeAmount", new Commission().dao().findTodayDistributeAmount(getCurrentMember().getId()));
		setAttr("totalDistributeAmount", new Commission().dao().findTotalDistributeAmount(getCurrentMember().getId()));
		setAttr("noPayNum", new Order().dao().findCount(getCurrentMember().getId(),Order.Status.PENDING_PAYMENT.ordinal()));
		setAttr("fansCount", new Member().dao().findFansCount(getCurrentMember().getId()));
		setAttr("member", new Member().dao().findById(getCurrentMember().getId()));
	    render("/templates/"+getTheme()+"/"+getDevice()+"/member.html");
	}

	
   /**
     * 编辑
     */
    public void edit() {
        setAttr("member", new Member().dao().findById(getCurrentMember().getId()));
        render("/templates/"+getTheme()+"/"+getDevice()+"/member_edit.html");
    }
    
    /**
     * 更新
     */
    public void update(){
        Member member = getModel(Member.class,"",true);
        Member pMember = new Member().dao().findById(getCurrentMember().getId());
        pMember.setAddress(member.getAddress());
        pMember.setName(member.getName());
        pMember.setModifyDate(new Date());
        pMember.update();
        renderJson(Feedback.success(new HashMap<>()));
    }
}
