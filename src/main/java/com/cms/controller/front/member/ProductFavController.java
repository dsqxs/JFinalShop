package com.cms.controller.front.member;

import java.util.HashMap;
import java.util.List;

import com.cms.Feedback;
import com.cms.controller.front.BaseController;
import com.cms.entity.Member;
import com.cms.entity.Product;
import com.cms.entity.ProductFav;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/member/product_fav")
public class ProductFavController extends BaseController{

	public void index(){
		Member currentMember = getCurrentMember();
		List<Product> products = new Product().dao().findFavList(currentMember.getId());
		setAttr("products", products);
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_product_fav.html");
	}
	
	/**
	 * 列表
	 */
	public void list(){
		Integer pageNumber = getParaToInt("pageNumber");
		if(pageNumber==null){
			pageNumber=1;
		}
		int pageSize = 10 ; 
		Member currentMember = getCurrentMember();
		setAttr("page",new Product().dao().findFavPage(pageNumber,pageSize,currentMember.getId()));
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_product_fav_list.html");
	}
	
	public void delete(){
		Long productId = getParaToLong("productId");
		Member currentMember = getCurrentMember();
		ProductFav productFav = new ProductFav().dao().findByMemberId(currentMember.getId(), productId);
		productFav.delete();
		renderJson(Feedback.success(new HashMap<>()));
	}
}
