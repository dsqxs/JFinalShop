package com.cms.controller.front.member;

import com.cms.controller.front.BaseController;
import com.cms.entity.Commission;
import com.cms.entity.Member;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/member/commission")
public class CommissionController extends BaseController{
	
	public void index(){
		setAttr("commissions", new Commission().dao().findList(getCurrentMember().getId(), null));
		setAttr("todayDistributeAmount", new Commission().dao().findTodayDistributeAmount(getCurrentMember().getId()));
		setAttr("monthDistributeAmount", new Commission().dao().findMonthDistributeAmount(getCurrentMember().getId()));
		setAttr("totalDistributeAmount", new Commission().dao().findTotalDistributeAmount(getCurrentMember().getId()));
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_commission.html");
	}

	/**
	 * 列表
	 */
	public void list(){
		Integer pageNumber = getParaToInt("pageNumber");
		if(pageNumber==null){
			pageNumber=1;
		}
		int pageSize = 10 ; 
		Member currentMember = getCurrentMember();
		setAttr("page",new Commission().dao().findShouyiPage(pageNumber,pageSize,currentMember.getId()));
		setAttr("todayDistributeAmount", new Commission().dao().findTodayDistributeAmount(getCurrentMember().getId()));
		setAttr("monthDistributeAmount", new Commission().dao().findMonthDistributeAmount(getCurrentMember().getId()));
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_commission_list.html");
	}
	
	/**
	 * 
	 */
	public void toBalance(){
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_commission_toBalance.html");
	}
	
}
