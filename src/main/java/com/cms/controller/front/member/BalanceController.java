package com.cms.controller.front.member;

import com.cms.controller.front.BaseController;
import com.cms.entity.Member;
import com.cms.entity.Payment;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/member/balance")
public class BalanceController extends BaseController{
	
	/**
	 * 列表
	 */
	public void list(){
		Integer pageNumber = getParaToInt("pageNumber");
		if(pageNumber==null){
			pageNumber=1;
		}
		int pageSize = 10 ; 
		Member currentMember = getCurrentMember();
		setAttr("page",new Payment().dao().findPage(currentMember.getId(),pageNumber,pageSize));
		setAttr("member", new Member().dao().findById(getCurrentMember().getId()));
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_balance_list.html");
	}
	
	
	public void index(){
		setAttr("payments", new Payment().dao().findByMemberId(getCurrentMember().getId()));
		setAttr("member", new Member().dao().findById(getCurrentMember().getId()));
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_balance.html");
	}

}
