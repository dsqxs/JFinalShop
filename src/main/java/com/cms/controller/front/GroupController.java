package com.cms.controller.front;

import com.cms.entity.Group;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/group")
public class GroupController extends BaseController{

	/**
	 * 详情页
	 */
	public void detail(){
		Long groupId=getParaToLong(0);
		Group group = new Group().dao().findById(groupId);
        setAttr("group", group);
		render("/templates/"+getTheme()+"/"+getDevice()+"/group_detail.html");
	}
	
}
