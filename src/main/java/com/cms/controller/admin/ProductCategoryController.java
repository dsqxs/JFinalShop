package com.cms.controller.admin;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cms.Feedback;
import com.cms.entity.Product;
import com.cms.entity.ProductCategory;
import com.cms.routes.RouteMapping;
import com.jfinal.plugin.activerecord.Db;


/**
 * Controller - 商品分类
 * 
 * 
 * 
 */
@RouteMapping(url = "/admin/product_category")

public class ProductCategoryController extends BaseController{
	
	
	public void setprice(){
		Long id = getParaToLong("id");
		setAttr("id", id);
		render(getView("product_category/setprice"));
	}
	
	public void updateprice(){
		Long productCategoryId = getParaToLong("id");
		String pricerate = getPara("pricerate");
		String costrate = getPara("costrate");
		String marketPricerate = getPara("marketPricerate");
		String filterSql = "";
        if(productCategoryId!=null){
		    filterSql+=" and (productCategoryId="+productCategoryId+" or productCategoryId in ( select id from cms_product_category where treePath  like '%"+ProductCategory.TREE_PATH_SEPARATOR+productCategoryId+ProductCategory.TREE_PATH_SEPARATOR+"%'))";
		}
        Db.update("update cms_product set price=oldprice*"+pricerate+",cost=oldprice*"+costrate+",marketPrice=oldprice*"+marketPricerate+" where 1=1 "+filterSql);
		renderJson(Feedback.success(new HashMap<>()));
	}

    /**
     * 添加
     */
    public void add(){
        setAttr("productCategoryTree", new ProductCategory().dao().findTree());
        render(getView("product_category/add"));
    }
    
    /**
     * 保存
     */
    public void save(){
        ProductCategory productCategory = getModel(ProductCategory.class,"",true);
        productCategory.setCreateDate(new Date());
        productCategory.setModifyDate(new Date());
        productCategory.setValue();
        productCategory.save();
        redirect(getListQuery("/admin/product_category/list"));
    }
    
    /**
     * 编辑
     */
    public void edit(){
        setAttr("productCategory", new ProductCategory().dao().findById(getParaToLong("id")));
        setAttr("productCategoryTree", new ProductCategory().dao().findTree());
        render(getView("product_category/edit"));
    }

    /**
     * 更新
     */
    public void update(){
        ProductCategory productCategory = getModel(ProductCategory.class,"",true);
        productCategory.setModifyDate(new Date());
        productCategory.update();
        productCategory.setValue();
        redirect(getListQuery("/admin/product_category/list"));
    }
    
    /**
     * 修改排序
     */
    public void updateSort(){
        String sortArray = getPara("sortArray");
        JSONArray jsonArray = JSONArray.parseArray(sortArray);
        for(int i=0;i<jsonArray.size();i++){
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Long id = jsonObject.getLong("id");
            Integer sort = jsonObject.getInteger("sort");
            ProductCategory productCategory = new ProductCategory().dao().findById(id);
            productCategory.setSort(sort);
            productCategory.update();
        }
        renderJson(Feedback.success(new HashMap<>()));
    }
    
    /**
     * 列表
     */
    public void list(){
        setAttr("productCategoryTree", new ProductCategory().dao().findTree());
        render(getView("product_category/list"));
    }
    
    /**
     * 删除
     */
    public void delete(){
        Long id = getParaToLong("id");
        ProductCategory productCategory = new ProductCategory().dao().findById(id);
        if (productCategory == null) {
            renderJson(Feedback.error("分类不存在"));
            return;
        }
        List<ProductCategory> children = productCategory.getChildren();
        if (children != null && !children.isEmpty()) {
            renderJson(Feedback.error("存在下级分类，无法删除"));
            return;
        }
        List<Product> products = productCategory.getProducts();
        if (products != null && !products.isEmpty()) {
            renderJson(Feedback.error("存在下级内容，无法删除"));
            return;
        }
        new ProductCategory().dao().deleteById(id);
        renderJson(Feedback.success(new HashMap<>()));
    }
}