package com.cms.controller.admin;

import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang.ArrayUtils;

import com.cms.Feedback;
import com.cms.entity.Seckill;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/admin/seckill")
public class SeckillController extends BaseController {

	/**
	 * 添加
	 */
	public void add() {
		render(getView("seckill/add"));
	}

	/**
	 * 保存
	 */
	public void save() {
		Seckill seckill = getModel(Seckill.class,"",true); 
		seckill.setCreateDate(new Date());
		seckill.setModifyDate(new Date());
		seckill.save();
		redirect(getListQuery("/admin/seckill/list"));
	}

	/**
	 * 编辑
	 */
	public void edit() {
		Long id = getParaToLong("id");
		setAttr("seckill", new Seckill().dao().findById(id));
		render(getView("seckill/edit"));
	}

	/**
	 * 更新
	 */
	public void update() {
		Seckill seckill = getModel(Seckill.class,"",true); 
		seckill.setModifyDate(new Date());
		seckill.update();
		redirect(getListQuery("/admin/seckill/list"));
	}
	
	/**
	 * 列表
	 */
	public void list() {
	    String name = getPara("name");
		Integer pageNumber = getParaToInt("pageNumber");
		if(pageNumber==null){
			pageNumber = 1;
		}
		setAttr("page", new Seckill().dao().findPage(name,pageNumber,PAGE_SIZE));
		setAttr("name", name);
		render(getView("seckill/list"));
	}

	/**
	 * 删除
	 */
	public void delete() {
		Long ids[] = getParaValuesToLong("ids");
		if(ArrayUtils.isNotEmpty(ids)){
			for(Long id:ids){
				new Seckill().dao().deleteById(id);
			}
		}
		renderJson(Feedback.success(new HashMap<>()));
	}
}
