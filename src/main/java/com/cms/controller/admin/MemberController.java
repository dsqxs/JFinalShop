package com.cms.controller.admin;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import com.cms.Feedback;
import com.cms.entity.Member;
import com.cms.entity.MemberRank;
import com.cms.entity.Order;
import com.cms.entity.Payment;
import com.cms.routes.RouteMapping;


/**
 * Controller - 会员
 * 
 * 
 * 
 */
@RouteMapping(url = "/admin/member")

public class MemberController extends BaseController{

	/**
	 * 添加
	 */
	public void add(){
		setAttr("memberRanks", new MemberRank().dao().findAll());
	    render(getView("member/add"));
	}
	
	/**
	 * 保存
	 */
	public void save(){
		Member member = getModel(Member.class,"",true);
		member.setPassword(DigestUtils.md5Hex(member.getPassword()));
		member.setCreateDate(new Date());
		member.setModifyDate(new Date());
		member.save();
		redirect(getListQuery("/admin/member/list"));
	}
	
	
	/**
	 * 编辑
	 */
	public void edit(){
		Long id = getParaToLong("id");
		setAttr("member", new Member().dao().findById(id));
		setAttr("memberRanks", new MemberRank().dao().findAll());
		render(getView("member/edit"));
	}
	
	/**
	 * 更新
	 */
	public void update(){
		Member member = getModel(Member.class,"",true); 
		if(member.getParentId()!=null){
			member.setValue();
		}
		member.setModifyDate(new Date());
		member.update();
		redirect(getListQuery("/admin/member/list"));
	}
	
	/**
	 * 更新
	 */
	public void updatePassword(){
	    Long id = getParaToLong("id");
	    String password = getPara("password");
	    Member member = new Member().dao().findById(id);
	    if(StringUtils.isNotBlank(password)){
	        member.setPassword(DigestUtils.md5Hex(password));
	        member.update();
	    }
		redirect(getListQuery("/admin/member/list"));
	}
	
	/**
	 * 列表
	 */
	public void list(){
		String nickname = getPara("nickname");
	    String mobile = getPara("mobile");
	    Integer pageNumber = getParaToInt("pageNumber");
        if(pageNumber==null){
            pageNumber = 1;
        }
		setAttr("page", new Member().dao().findPage(mobile,nickname,pageNumber,PAGE_SIZE));
		setAttr("mobile", mobile);
		setAttr("nickname", nickname);
		render(getView("member/list"));
	}
	
	public void choose(){
		String nickname = getPara("nickname");
	    String mobile = getPara("mobile");
	    Integer pageNumber = getParaToInt("pageNumber");
        if(pageNumber==null){
            pageNumber = 1;
        }
		setAttr("page", new Member().dao().findPage(mobile,nickname,pageNumber,PAGE_SIZE));
		setAttr("mobile", mobile);
		setAttr("nickname", nickname);
		render(getView("member/choose"));
	}
	
	public void editBalance(){
		Long memberId = getParaToLong("memberId");
		String type = getPara("type");
		setAttr("type", type);
		setAttr("memberId", memberId);
		setAttr("member", new Member().dao().findById(memberId));
		render(getView("member/editBalance"));
	}
	
	public void updateBalance(){
		Long memberId = getParaToLong("memberId");
		String type = getPara("type");
		BigDecimal money = getParaToBigDecimal("money");
		if("add".equals(type)){
			Payment payment = new Payment();
			payment.setType(Payment.Type.SYSTEM.ordinal());
			payment.setCreateDate(new Date());
			payment.setModifyDate(new Date());
			payment.setMemberId(memberId);
			payment.setOrderId(null);
			payment.setAmount(money);
			payment.setInout("+");
			payment.save();
			
			
			Member member = payment.getMember();
			BigDecimal newAmount = member.getAmount().add(money);
			member.setAmount(newAmount);
			member.update();
		}else if("reduce".equals(type)){
			Payment payment = new Payment();
			payment.setType(Payment.Type.SYSTEM.ordinal());
			payment.setCreateDate(new Date());
			payment.setModifyDate(new Date());
			payment.setMemberId(memberId);
			payment.setOrderId(null);
			payment.setAmount(money);
			payment.setInout("-");
			payment.save();
			
			
			Member member = payment.getMember();
			BigDecimal newAmount = member.getAmount().subtract(money);
			member.setAmount(newAmount);
			member.update();
		}
		renderJson(Feedback.success(new HashMap<>()));
	}
	
	
	/**
	 * 列表
	 */
	public void balance(){
	    Long memberId = getParaToLong("memberId");
	    Integer pageNumber = getParaToInt("pageNumber");
        if(pageNumber==null){
            pageNumber = 1;
        }
		setAttr("page", new Payment().dao().findPage(memberId,pageNumber,PAGE_SIZE));
		setAttr("memberId", memberId);
		render(getView("member/balance"));
	}
	
	/**
	 * 订单
	 */
	public void order(){
		Long memberId = getParaToLong("memberId");
	    Integer pageNumber = getParaToInt("pageNumber");
        if(pageNumber==null){
            pageNumber = 1;
        }
        setAttr("page", new Order().dao().findPage(null,null,null,memberId,null,pageNumber,PAGE_SIZE));
		setAttr("memberId", memberId);
		render(getView("member/order"));
	}
	
	/**
	 * 删除
	 */
	public void delete(){
		Long ids[] = getParaValuesToLong("ids");
		for(Long id:ids){
			new Member().dao().deleteById(id);
		}
		renderJson(Feedback.success(new HashMap<>()));
	}
}
