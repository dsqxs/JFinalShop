package com.cms.entity;

import java.util.List;

import com.cms.entity.base.BaseMessageConfig;

/**
 * Entity - 消息配置
 * 
 * 
 * 
 */
public class MessageConfig extends BaseMessageConfig<MessageConfig> {
	
	private static final long serialVersionUID = -878152889660463735L;
	
	/**
	 * 类型
	 */
	public enum Type{
		REGISTER("会员注册"),
		FORGET_PASSWORD("忘记密码"),
		UPDATE_PASSWORD("修改密码"),
		SET_ACCOUNT("设置账户"),
		LOGIN("会员登录");
		public String text;
		Type(String text){
			this.text = text;
		}
		public String getText(){
			return this.text;
		} 
	}
	
	/**
	 * 查找所有消息配置
	 * 
	 * @return 所有消息配置
	 */
	public List<MessageConfig> findAll(){
		return find("select * from cms_message_config");
	}
	
	/**
	 * 根据类型查找消息配置
	 * 
	 * @param type
	 *           类型
	 * @return 消息配置
	 */
	public MessageConfig findByType(Integer type) {
		if (type == null) {
			return null;
		}
		String sql = "select * from cms_message_config where type = ?";
		return findFirst(sql,type);
	}
}
