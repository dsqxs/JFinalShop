package com.cms.entity.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseGroupProduct<M extends BaseGroupProduct<M>> extends Model<M> implements IBean {

	public M setId(Long id) {
		set("id", id);
		return (M)this;
	}
	
	public Long getId() {
		return getLong("id");
	}
	
	public M setCreateDate(java.util.Date createDate) {
		set("createDate", createDate);
		return (M)this;
	}
	
	public java.util.Date getCreateDate() {
		return get("createDate");
	}
	
	public M setModifyDate(java.util.Date modifyDate) {
		set("modifyDate", modifyDate);
		return (M)this;
	}
	
	public java.util.Date getModifyDate() {
		return get("modifyDate");
	}
	
	public M setStatus(Integer status) {
		set("status", status);
		return (M)this;
	}
	
	public Integer getStatus() {
		return getInt("status");
	}
	
	public M setCurrentNum(Integer currentNum) {
		set("currentNum", currentNum);
		return (M)this;
	}
	
	public Integer getCurrentNum() {
		return getInt("currentNum");
	}
	
	public M setNum(Integer num) {
		set("num", num);
		return (M)this;
	}
	
	public Integer getNum() {
		return getInt("num");
	}
	
	public M setProductId(Long productId) {
		set("productId", productId);
		return (M)this;
	}
	
	public Long getProductId() {
		return getLong("productId");
	}
	
	public M setPrice(java.math.BigDecimal price) {
		set("price", price);
		return (M)this;
	}
	
	public java.math.BigDecimal getPrice() {
		return get("price");
	}
	
	public M setSort(Integer sort) {
		set("sort", sort);
		return (M)this;
	}
	
	public Integer getSort() {
		return getInt("sort");
	}
	
	public M setGroupId(Long groupId) {
		set("groupId", groupId);
		return (M)this;
	}
	
	public Long getGroupId() {
		return getLong("groupId");
	}
	
}
