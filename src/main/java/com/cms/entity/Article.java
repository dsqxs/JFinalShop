package com.cms.entity;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;

import com.cms.entity.base.BaseArticle;
import com.cms.util.DBUtils;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.Page;

/**
 * Entity - 文章
 * 
 * 
 * 
 */
@SuppressWarnings("serial")
public class Article extends BaseArticle<Article> {
    
    /**
     * 文章分类
     */
    private ArticleCategory articleCategory;
    
    
    /**
     * 获取文章分类
     * @return 文章分类
     */
    public ArticleCategory getArticleCategory(){
        if(articleCategory == null){
            articleCategory = new ArticleCategory().dao().findById(getArticleCategoryId());
        }
        return articleCategory;
    }
    
    
   /**
     * 查找文章分页
     * 
     * @param articleCategoryId
     *            文章分类Id
     * @param pageable
     *            分页信息
     * @param siteId
     *            站点id            
     * @return 文章分页
     */
    public Page<Article> findPage(Long articleCategoryId,String title,Integer pageNumber,Integer pageSize){
        String filterSql = "";
        if(articleCategoryId!=null){
            filterSql+=" and (articleCategoryId="+articleCategoryId+" or articleCategoryId in ( select id from cms_article_category where treePath  like '%"+ArticleCategory.TREE_PATH_SEPARATOR+articleCategoryId+ArticleCategory.TREE_PATH_SEPARATOR+"%'))";
        }
        if(StringUtils.isNotBlank(title)){
            filterSql+= " and title like '%"+title+"%'";
        }
        String orderBySql = DBUtils.getOrderBySql("createDate desc");
        return paginate(pageNumber, pageSize, "select *", "from cms_article where 1=1 "+filterSql+orderBySql);
    }
    
    /**
     * 查找文章分页
     * 
     * @param articleCategoryId
     *            文章分类Id
     * @param pageNumber
     *            页码
     * @param pageSize
     *            每页记录数
     * @param order
     *            排序
     * @param direction
     *            方向
     * @param siteId
     *            站点id                 
     * @return 文章分页
     */
    public Page<Article> findPage(Long articleCategoryId,Integer pageNumber,Integer pageSize,String orderBy){
        String filterSql = "";
        if(articleCategoryId!=null){
            filterSql+=" and (articleCategoryId="+articleCategoryId+" or articleCategoryId in ( select id from cms_article_category where treePath  like '%"+ArticleCategory.TREE_PATH_SEPARATOR+articleCategoryId+ArticleCategory.TREE_PATH_SEPARATOR+"%'))";
        }
        String orderBySql = "";
        if(StringUtils.isNotBlank(orderBy)){
            orderBySql = DBUtils.getOrderBySql(orderBy);
        }else{
            orderBySql = DBUtils.getOrderBySql("createDate desc");
        }
        return paginate(pageNumber, pageSize, "select *", "from cms_article where 1=1 "+filterSql+orderBySql);
    }
    
    /**
     * 查找文章列表
     * 
     * @param articleCategoryId
     *            文章分类Id
     * @param first
     *            起始记录
     * @param count
     *            数量
     * @param beginDate
     *            起始日期
     * @param endDate
     *            结束日期
     * @param filters
     *            筛选
     * @param orderBy
     *            排序
     * @param siteId
     *            站点id                 
     * @return 文章列表
     */
    public List<Article> findList(Long articleCategoryId,Integer first,Integer count,String orderBy){
        String filterSql = "";
        if(articleCategoryId!=null){
            filterSql+=" and (articleCategoryId="+articleCategoryId+" or articleCategoryId in ( select id from cms_article_category where treePath  like '%"+ArticleCategory.TREE_PATH_SEPARATOR+articleCategoryId+ArticleCategory.TREE_PATH_SEPARATOR+"%'))";
        }
        String orderBySql = "";
        if(StringUtils.isNotBlank(orderBy)){
            orderBySql = DBUtils.getOrderBySql(orderBy);
        }else{
            orderBySql = DBUtils.getOrderBySql("createDate desc");
        }
        String countSql=DBUtils.getCountSql(first, count);
        return find("select * from cms_article where 1=1 "+filterSql+orderBySql+countSql);
    }
    
    
    /**
     * 获取路径
     * 
     * @return 路径
     */
    public String getPath() {
        return JFinal.me().getContextPath()+"/article/detail/"+getId();
    }

    /**
     * 获取文本内容
     * 
     * @return 文本内容
     */
    public String getText() {
        if (StringUtils.isEmpty(getContent())) {
            return StringUtils.EMPTY;
        }
        return Jsoup.parse(getContent()).text();
    }
}
